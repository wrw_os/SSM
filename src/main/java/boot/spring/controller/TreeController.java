package boot.spring.controller;

import java.util.ArrayList;
import java.util.List;

import boot.spring.po.LableInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import boot.spring.bootstrap.StarterRun;
import boot.spring.pagemodel.AjaxResult;
import boot.spring.pagemodel.DwNode;
import boot.spring.pagemodel.XzqhNode;
import boot.spring.po.Dwb;
import boot.spring.po.Xzqh;
import boot.spring.service.DwService;
import boot.spring.service.XzqhService;
import boot.spring.tools.TreeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "树状结构生成接口")
@Controller
public class TreeController {
	
	@Autowired
	XzqhService xzqhService;
	
	@Autowired
    DwService dwService;
	
	@ApiOperation("生成单位树状结构")
	@RequestMapping(value="/DWTree",method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult DWTree(){
		System.out.println("===========初始化单位树开始=========");
        long startTime = System.currentTimeMillis();
        // 查询所有单位
        List<Dwb> dwblist = dwService.listDwb();
        List<DwNode> nodes = new ArrayList<>();
        // 把单位数据转化为节点对象列表
        dwblist.stream().forEach(dw->{
        	DwNode node = dwService.transformDwbToNode(dw);
        	nodes.add(node);
        });
        // 将节点列表转化为节点树，从根节点开始递归构建
        DwNode tree = TreeUtils.buildTree(nodes);
        long endTime = System.currentTimeMillis();
        System.out.println("初始化单位结束,耗时"+(endTime-startTime));
        return AjaxResult.success(tree);
	}
	
	@ApiOperation("生成行政区划树状结构")
	@RequestMapping(value="/XZQHTree",method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult XZQHTree(){
		System.out.println("===========初始化行政区划树开始=========");
        long startTime = System.currentTimeMillis();
        // 查询所有行政区
        List<Xzqh> xzqlist = xzqhService.listXzqh();
        List<XzqhNode> nodes = new ArrayList<>();
        // 把PO数据转化为节点对象列表
        xzqlist.stream().forEach(xzqh->{
        	XzqhNode node = xzqhService.transformXzqhToNode(xzqh);
        	nodes.add(node);
        });
        // 将节点列表转化为节点树，从根节点开始递归构建
        List<XzqhNode> tree = TreeUtils.buildMultiTree(nodes);
        long endTime = System.currentTimeMillis();
        System.out.println("初始化行政区划树结束,耗时"+(endTime-startTime));
		return AjaxResult.success(tree);
	}


	@RequestMapping(value="/tree",method = RequestMethod.GET)
	@ResponseBody
	public List<LableInfo> tree() {
		List<LableInfo> list  = new ArrayList<>();
		list.add(new LableInfo(1, null, "项目指标", 2));
		list.add(new LableInfo(2, null, "技术指标", 5));
		list.add(new LableInfo(3, null, "产出指标", 6));
		list.add(new LableInfo(4, 1, "项目1", 3));
		list.add(new LableInfo(5, 2, "技术1", 1));
		list.add(new LableInfo(6, 3, "产出指标1", 1));
		list.add(new LableInfo(7, 3, "产出指标2", 1));
		list.add(new LableInfo(8, 4, "项目12", 5));
		list.add(new LableInfo(9, 4, "项目13", 3));
		list.add(new LableInfo(10, 8, "项目123", 2));

		// 先找到根节点，然后循环建树
		List<LableInfo> roots = getRoots(list);
		// 最终结果
		List<LableInfo> treeMenus = new ArrayList<>();
		roots.stream().forEach(r->{
			LableInfo tree = buildTree(r, list);
			// 递归统计
			caculateValue(tree);
			treeMenus.add(tree);
		});
		return treeMenus;
	}

	// 获取根节点列表
	List<LableInfo> getRoots(List<LableInfo> list) {
		List<LableInfo> roots  = new ArrayList<>();
		list.stream().forEach(l->{
			if (l.getPid() == null) {
				roots.add(l);
			}
		});
		return roots;
	}

	// 从根节点建树
	LableInfo buildTree(LableInfo root, List<LableInfo> list) {
		List<LableInfo> children = new ArrayList<>();
		list.stream().forEach(l->{
			if (l.getPid().equals(root.getId())) {
				children.add(buildTree(l, list));
			}
		});
		root.setChildren(children);
		return root;
	}

	// 递归相加
	Integer caculateValue(LableInfo lableInfo) {
		Integer value = lableInfo.getValue();
		if (lableInfo.getChildren() == null || (lableInfo.getChildren() != null && lableInfo.getChildren().size() == 0)) {
			return value;
		}
		for (int i = 0; i < lableInfo.getChildren().size(); i++) {
			value += caculateValue(lableInfo.getChildren().get(i));
		}
		lableInfo.setValue(value);
		return value;
	}
}
